{
        'id':0,
        'text':'Fear of blood or needles',
        'icon' : 'icon_blooddrop.png',
        'selected' : true,
        'response' : 'Some people aren’t comfortable around blood or needles. If this sounds like you, read on to learn some easy ways to reduce your discomfort.',
        'solutionsSelected' : 2,
        'defaultSolution' : 'Distract myself with music, texting, reading, or talking to staff or other donors.',
        'bestSolution' : null,
        'solutions' : [
          {
            'id' : 0,
            'text' : 'Distract yourself with music, texting, reading, or talking to staff or other donors.',
            'selected' : true,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about fear of needles or blood, then I will distract myself with music, texting, reading, or talking to staff or other donors.'
          },
          {
            'id' : 1,
            'text' : 'Express your concern to the staff when you arrive, so they can help you.',
            'selected' : true,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about fear of needles or blood, then I will express my concern to the staff when I arrive, so they can help me.'
          },
          {
            'id' : 2,
            'text' : 'Ask for an experienced staff member to draw your blood.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about fear of needles or blood, then I will ask for an experienced staff member to draw my blood.'
          },
          {
            'id' : 3,
            'text' : 'Tell the staff which arm you prefer.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about fear of needles or blood, then I will tell the staff which arm I prefer.'
          },
          {
            'id' : 4,
            'text' : 'Bring someone for support.',
            'selected'  : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about fear of needles or blood, then I will bring someone for support.'
          },
          {
            'id' : 5,
            'text' : 'Schedule an appointment at a less busy time to shorten your wait.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about fear of needles or blood, then I will schedule an appointment at a less busy time to shorten my wait.'
          }
        ]
      },
      {
        'id':1,
        'text':'Feeling faint, dizzy, or lightheaded',
        'icon' : 'icon_dizzy.png',
        'selected' : true,
        'response' : 'More than 95% of donors give blood without any physical symptoms. Among the small percentage who do, the most common symptom is mild faintness that quickly goes away. If you’re concerned about having such symptoms, read on to learn simple ways prevent them.',
        'solutionsSelected' : 0,
        'bestSolution' : null,
        'defaultSolution' : 'Tense my leg and stomach muscles every 10 seconds while donating.',
        'solutions' : [
          {
            'id' : 0,
            'text' : 'Drink a bottle of water 10 minutes before you donate. Water causes a short-term blood pressure increase that helps prevent dizziness and faintness. It also helps replace the fluid that you lose during donation.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about feeling faint, dizzy, or lightheaded, then I will drink a bottle of water 10 minutes before I donate.'
          },
          {
            'id' : 1,
            'text' : 'Tense your leg and stomach muscles once every 10 seconds while donating. Crossing your legs and tensing your leg and stomach muscles helps prevent faintness and dizziness by keeping blood pressure in a normal range.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about feeling faint, dizzy, or lightheaded, then I will tense my leg and stomach muscles once every 10 seconds while donating.'
          },
        ]
      
      },
      {
        'id':2,
        'text':'Finding a time to donate',
        'icon' : 'icon_time.png',
        'selected' : true,
        'response' : 'It can be challenging to find time to donate in a busy schedule. Read on for some tips to make it easier.',
        'solutionsSelected' : 0,
        'bestSolution' : null,
        'defaultSolution' : 'Donate at a location that takes appointments to reduce my wait time.',
        'solutions' : [
          {
            'id' : 0,
            'text' : 'Donate at a location that takes appointments (as this can shorten your wait time).',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about finding a time to donate, then I will donate at a location that takes appointments.'
          },
          {
            'id' : 1,
            'text' : 'Book an appointment well in advance at NYBC.org or using the NYBC mobile app.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about finding a time to donate, then I will book an appointment well in advance at NYBC.org or using the NYBC mobile app.'
          },
          {
            'id' : 2,
            'text' : 'Create a personal reminder on your calendar.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about finding a time to donate, then I will create a personal reminder on my calendar.'
          },
        ]
      },
      {
        'id':3,
        'text':'Finding a place to donate',
        'icon' : 'icon_donationlocation.png',
        'selected' : true,
        'response' : 'If you aren’t sure how to find a donation site near your home, work or school, read on for a great tip to make this easy!',
        'solutionsSelected' : 0,
        'bestSolution' : null,
        'defaultSolution' : 'Go to NYBC.org to find an upcoming blood drive.',
        'solutions' : [
          {
            'id' : 0,
            'text' : 'Go to NYBC.org to find an upcoming blood drive.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about finding a place to donate, then I will go to NYBC.org to find an upcoming blood drive.'
          },
          {
            'id' : 1,
            'text' : 'Download the NYBC mobile app to find an upcoming blood drive.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about finding a place to donate, then I will download the NYBC mobile app to find an upcoming blood drive.'
          },
        ]
      },
      {
        'id':4,
        'text':'Feeling tired after donating',
        'icon' : 'icon_sleep.png',
        'selected' : true,
        'response' : 'Some people feel tired after donating blood. The good news is that there is an easy way to prevent this symptom. Read on to find out more!',
        'solutionsSelected' : 0,
        'bestSolution' : null,
        'defaultSolution' : 'Drink plenty of water and eat salty foods on the day before I donate to help my body store up fluids.',
        'solutions' : [
          {
            'id' : 0,
            'text' : 'Drink plenty of fluid and consume salty foods on the day before you donate. This helps to store fluid.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about feeling tired after donating, then I will drink plenty of fluid and consume salty foods on the day before I donate.'
          },
          {
            'id' : 1,
            'text' : 'Drink plenty of fluid on the day of your donation, both before and after you give blood.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about feeling tired after donating, then I will drink plenty of fluid on the day of my donation, both before and after I give blood.'
          },
          {
            'id' : 2,
            'text' : 'Avoid alcohol and caffeine on the day of your donation as these dehydrate your body.',
            'selected' : null,
            'isBest' : false,
            'bestSolutionText' : 'If I am concerned about feeling tired after donating, then I will avoid alcohol and caffeine on the day of my donation as these dehydrate my body.'
          },
        ]
      }