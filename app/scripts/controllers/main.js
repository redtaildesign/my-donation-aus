'use strict';

angular.module('careApp')
.controller('MainCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', 'manageFactory', function ($scope, $location, donorIDFactory, concernsFactory, manageFactory) {

  var currentPage;

  var init = function(){
    console.log('init');
    $scope.loggedIn = donorIDFactory.loggedIn();

    // If there is a current session var, load the data for that user
    if(!$scope.loggedIn && sessionStorage.getItem("donorID")){
      console.log("session found: " + sessionStorage.getItem("donorID"));
      $scope.donorID = sessionStorage.getItem("donorID");
      loadRemoteData();
    }

    concernsFactory.setCurrentPage($location.path().substring(1) || 0);

    // $scope.loggedIn;
    $scope.questions = concernsFactory.all();

  };

  function loadRemoteData() {
      console.log('loading donor id: ' + $scope.donorID);

      //get the donor ID's data
      donorIDFactory.get($scope.donorID)
      .then(function(newID){
        //update the view
        var theData = newID.donor[0];
        if(theData){
          console.log("successful");
          donorIDFactory.logIn($scope.donorID, theData.id);
          $scope.loggedIn = donorIDFactory.loggedIn();
          $scope.nextDisabled = false;

          // //update the location
          // donorIDFactory.setLocation(theData.location);

          //update the local data
          concernsFactory.update(theData);
        }
        else{
          console.log("failed");
          $scope.logInFailed = true;
          $location.path('/');
        }
      });
    }

    // Page change
    $scope.$on('$routeChangeSuccess', function () {
      concernsFactory.setCurrentPage($location.path().substring(1) || 0);
      currentPage = concernsFactory.getCurrentPage();

      $scope.nextDisabled = false;

      console.log('currentPage: ' + currentPage);

      for(var question in $scope.questions){

        if($scope.questions[question].id == currentPage){
          $scope.questionNum = question;
          $scope.question = $scope.questions[question];
          $scope.nextPage = $scope.question.next;
          $scope.prevPage = $scope.question.prev;
          $scope.canContinue = $scope.question.canContinue;
        }

      }

      window.scroll(0, 0);
    });

    $scope.$on('$viewContentLoaded', function(){
      $scope.loggedIn = donorIDFactory.loggedIn();
      $scope.manageLoggedIn = manageFactory.loggedIn();
      //loadRemoteData();

      // If the user isn't logged in, return them to the start
      if( !($scope.loggedIn || $scope.manageLoggedIn) && ( currentPage != 0 && currentPage != 'manage-login')){
        //console.log('not logged in!');
        if(sessionStorage.getItem("donorID")){
          console.log("session found: " + sessionStorage.getItem("donorID"));
          $scope.donorID = sessionStorage.getItem("donorID");
          loadRemoteData();
        }
        // $location.path('/');
      }

    });

    // Prev/Next Button Handler
    $scope.buttonClick = function(buttonType){

      if(buttonType === 'next'){
        //console.log('next: ' + $scope.nextPage);
        if($scope.canContinue){
          concernsFactory.setPageComplete(currentPage);
          concernsFactory.updateRemote();
          concernsFactory.setCurrentPage($scope.nextPage);
          $location.path('/' + $scope.nextPage);
        }
        else{
          $scope.nextDisabled = true;
        }
      }
      else if(buttonType === 'prev'){
        concernsFactory.setCurrentPage($scope.prevPage);
        $location.path('/' + $scope.prevPage);
      }

    };

    init();

  }])

  ////////////////////////////
  // Text Response Controller
  ////////////////////////////
  .controller('textCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {
    //console.log('text');

    $scope.theResponse = $scope.question.response;

    $scope.canContinueUpdate = function(){
      if($scope.theResponse){
        //console.log('update');
        $scope.$parent.canContinue = true;
        $scope.$parent.nextDisabled = false;

      }

    };

    $scope.submitClick = function(){

      if($scope.theResponse){
        $scope.$parent.canContinue = true;
        $scope.$parent.nextDisabled = false;
        $scope.question.response = $scope.theResponse;
      }

      $scope.buttonClick('next');
    };

  }])


  ////////////////////////////
  // Slider Controller
  ////////////////////////////
  .controller('sliderCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {
    //console.log('slider');

    var slider = new Slider('#ex1', {
      formatter: function(value) {
        return value;
      },
      tooltip: 'always',
      value : $scope.question.value
    });

      // With JQuery
      $("#ex1").on("slideStop", function(slideEvt) {
        $scope.question.value = slideEvt.value;
        // //console.log("value: " + $scope.question.value);
      // $("#ex6SliderVal").text(slideEvt.value);
    });


    }])

  ////////////////////////////
  // Slider Text Response Controller
  ////////////////////////////
  .controller('sliderTextCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {
    //console.log('text');

    $scope.theResponse = $scope.question.response;

    if($scope.theResponse){
      $scope.$parent.canContinue = true;
    }

    var theValue = $scope.questions[$scope.questionNum-1].value;
    //console.log(theValue);
    if(theValue >= 4 && theValue <= 7 ){
      //console.log('yes');
      $scope.$parent.nextPage = $scope.question.altNext;
    }

    $scope.canContinueUpdate = function(){
      $scope.$parent.canContinue = true;
      $scope.$parent.nextDisabled = false;
    };

    $scope.submitClick = function(){

      if($scope.theResponse !== ""){
        $scope.$parent.canContinue = true;
        $scope.$parent.nextDisabled = false;
        $scope.question.response = $scope.theResponse;

        $scope.buttonClick('next');
      }
      else{
        $scope.$parent.canContinue = false;
        $scope.$parent.nextDisabled = true;
      }
    };

  }])

  ////////////////////////////
  // Multiple Choice Controller
  ////////////////////////////
  .controller('multiCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {

    var currentPage = concernsFactory.getCurrentPage();
    $scope.noneOfTheAbove = false;


    $scope.solutionClick = function(optionNum){
      $scope.nextDisabled = false;
      $scope.noneOfTheAbove = false;

      if($scope.question.options[optionNum].selected == null || $scope.question.options[optionNum].selected == false){
        $scope.question.options[optionNum].selected = true;
      }
      else{
        $scope.question.options[optionNum].selected = false;
      }

    };

    $scope.noneClick = function(){
      $scope.nextDisabled = false;
      $scope.noneOfTheAbove = !$scope.noneOfTheAbove;
      //console.log('noneOfTheAbove: ' + $scope.noneOfTheAbove);
      for(var option in $scope.question.options){
        $scope.question.options[option].selected = false;
        $scope.$parent.canContinue = true;
        $scope.$parent.nextDisabled = false;

      }
    };

    // Prev/Next Button Handler
    $scope.buttonClick = function(buttonType){

      if(buttonType === 'next'){
        //console.log('next: ' + $scope.nextPage);

        // Check if we can continue
        $scope.$parent.canContinue = false;
        for(var option in $scope.question.options){
          if($scope.question.options[option].selected == true){

            $scope.$parent.canContinue = true;
            $scope.$parent.nextDisabled = false;
          }
        }

        //skip the response page
        if($scope.noneOfTheAbove){
          $scope.$parent.canContinue = true;

          concernsFactory.setPageComplete(currentPage);
          currentPage = $scope.nextPage;

          for(var question in $scope.questions){
            if($scope.questions[question].id == currentPage){
              $scope.nextPage = $scope.questions[question].next;
              //console.log('next: ' + $scope.nextPage);
            }

          }
        }

        if($scope.canContinue){
          concernsFactory.setPageComplete(currentPage);
          concernsFactory.updateRemote();
          concernsFactory.setCurrentPage($scope.nextPage);
          $location.path('/' + $scope.nextPage);
        }
        else{
          $scope.nextDisabled = true;
        }
      }
      else if(buttonType === 'prev'){
        concernsFactory.setCurrentPage($scope.prevPage);
        $location.path('/' + $scope.prevPage);
      }

    };

  }])

  ////////////////////////////
  // Response Controller
  ////////////////////////////
  .controller('responseCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {
    //console.log('Response');
  }])

  ////////////////////////////
  // Credits Controller
  ////////////////////////////
  .controller('creditCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {
    //console.log('text');

    console.log('done');
    concernsFactory.setCompleted();
    concernsFactory.updateRemote();

  }])



  ////////////////////////////
  // Login Controller
  ////////////////////////////
  .controller('loginCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function($scope, $location, donorIDFactory, concernsFactory){
    var init = function(){
      $scope.donorID = donorIDFactory.getID();
      $scope.logInFailed = false;
      $scope.loggedIn = donorIDFactory.loggedIn();
      // $scope.loggedIn = true;
    };

    $scope.submitClick = function(){
      loadRemoteData();
    };

    function loadRemoteData() {
      //console.log('loading donor id: ' + $scope.donorID);

      //get the donor ID's data
      donorIDFactory.get($scope.donorID)
      .then(function(newID){
        //update the view
        var theData = newID.donor[0];
        if(theData){
          donorIDFactory.logIn($scope.donorID, theData.id);
          $scope.loggedIn = donorIDFactory.loggedIn();
          $scope.nextDisabled = false;

          // //update the location
          // donorIDFactory.setLocation(theData.location);

          //update the local data
          concernsFactory.update(theData);
        }
        else{
          $scope.logInFailed = true;
        }
      });
    }

    init();

  }])


  ////////////////////////////
  // Concerns Controller
  ////////////////////////////
  .controller('concernsCtrl', ['$scope', '$location', 'concernsFactory', function ($scope, $location, concernsFactory) {

    $scope.concerns = concernsFactory.all();
    $scope.currentConcern = 0;

    // $scope.nextDisabled = true;

    $scope.radioClick = function(concernNum, option){
      $scope.concerns[concernNum].selected = option;
      //localStorage.selected = JSON.stringify( $scope.selected);
    };

    $scope.concernsClick = function(concernNum, option){
      $scope.concerns[concernNum].selected = option;
      //localStorage.selected = JSON.stringify( $scope.selected);
    };

    //
    // Special next/prev button behaviour
    //
    $scope.buttonClick = function(buttonType){
      var currentPage = parseInt($location.path().substring(1) || 1);
      //console.log($scope.currentConcern);

      if(buttonType === 'next'){
        if($scope.currentConcern < 4){
          $scope.currentConcern += 1;
        }
        else{

          concernsFactory.setPage(currentPage);
          concernsFactory.updateRemote();
          $location.path('/' + String(currentPage + 1));
        }
      }
      else if(buttonType === 'prev'){
        if($scope.currentConcern > 0){
          $scope.currentConcern -= 1;
        }
        else{
          $location.path('/' + String(currentPage - 1));
        }
      }
    };

  }])

  ///////////////////////////////
  // Solutions Controller
  ///////////////////////////////
  .controller('solutionsCtrl', ['$scope', '$location', 'concernsFactory', 'donorIDFactory', function ($scope, $location, concernsFactory, donorIDFactory) {
    var currentPage;
    var concernsQuestion = 17;
    $scope.currentConcern = 0;
    $scope.currentConcernIndex = 0;
    $scope.concernsSelected = [];
    $scope.canContinue = true;
    $scope.theLocation = donorIDFactory.getLocation();

    currentPage = concernsFactory.getCurrentPage();

    for(var i=0; i < $scope.questions[concernsQuestion].options.length; i++){
      if($scope.questions[concernsQuestion].options[i].selected === true){
        $scope.concernsSelected.push(i);
      }
    }

    if($scope.concernsSelected.length > 0){
      canContinueUpdate();
    }

    $scope.$on('$viewContentLoaded', function(){
      // if( !isConcernSelected($scope.currentConcernIndex)){
      //   nextConcern();
      // }
    });

    function isConcernSelected(concernNum){
      //console.log("is concern:" + concernNum + " selected? " + $scope.questions[concernsQuestion].options[concernNum].selected);
      return ($scope.questions[concernsQuestion].options[concernNum].selected);
    }

        //check if all possible solutions have a choice
        function canContinueUpdate(){
          var continueFlag = true;
          for(var option in $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].options){
            if($scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].options[option].selected === null){
              //console.log("still more to answer!");
              continueFlag = false;
            }
          }


          $scope.canContinue = continueFlag;
          $scope.nextDisabled = !continueFlag;
        }

        function nextConcern(){
          $scope.currentConcernIndex++;
          canContinueUpdate();
        }

        function prevConcern(){
          $scope.currentConcernIndex--;
        }

        $scope.solutionClick = function(solutionNum, option){
      // //console.log('solution: ' + solutionNum + ' set: ' + option);
      $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].options[solutionNum].selected = option;
      canContinueUpdate();
    };

    // Unique Prev/Next Button Handler
    $scope.buttonClick = function(buttonType){

      if(buttonType === 'next'){
        //If there's more concerns, go to the next one
        if( ($scope.currentConcernIndex < $scope.concernsSelected.length - 1) && $scope.canContinue){
          nextConcern();
        }
        else{
          //console.log('next: ' + $scope.nextPage);
          if($scope.canContinue){
            concernsFactory.setPageComplete(currentPage);
            concernsFactory.updateRemote();
            concernsFactory.setCurrentPage($scope.nextPage);
            $location.path('/' + $scope.nextPage);
          }
          else{
            $scope.nextDisabled = true;
          }

        }
      }
      else if(buttonType === 'prev'){

        //If there's more concerns, go to the next one
        if( $scope.currentConcernIndex > 0){
          prevConcern();
        }
        else{
          concernsFactory.setCurrentPage($scope.prevPage);
          $location.path('/' + $scope.prevPage);
        }
      }

    };

    $scope.returnClick = function(){
      concernsFactory.setCurrentPage('5');
      $location.path('/' + '5');
    };

    $scope.continueClick = function(){
      concernsFactory.setCurrentPage('8a');
      $location.path('/' + '8a');
    };

  }])

  /////////////////////////////////
  // Solutions Response Controller
  /////////////////////////////////
  .controller('solutionsResponseCtrl', ['$scope', '$location', 'concernsFactory', function ($scope, $location, concernsFactory) {
    var currentPage;
    var concernsQuestion = 17;
    $scope.currentConcernIndex = 0;
    $scope.concernsSelected = [];
    $scope.theResponse ="";
    $scope.canContinue = true;

    $scope.$on('$viewContentLoaded', function(){
      currentPage = concernsFactory.getCurrentPage();

      for(var i=0; i < $scope.questions[concernsQuestion].options.length; i++){
        //console.log(i);
        if($scope.questions[concernsQuestion].options[i].selected === true){
          $scope.concernsSelected.push(i);
          $scope.canContinue = false;
        }
      }


      if($scope.concernsSelected.length > 0){

        $scope.theResponse = $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].response;
        if($scope.theResponse !== ""){
          $scope.canContinueUpdate();
        }

      }

    });

    function isConcernSelected(concernNum){
      //console.log("is concern:" + concernNum + " selected? " + $scope.questions[concernsQuestion].options[concernNum].selected);
      return ($scope.questions[concernsQuestion].options[concernNum].selected);
    }

    //check if all possible solutions have a choice

    function nextConcern(){
      $scope.currentConcernIndex++;
    }

    function prevConcern(){
      $scope.currentConcernIndex--;
    }

    $scope.submitClick = function(){
      //console.log("submit");
      if($scope.theResponse){
        $scope.canContinue = true;
        $scope.nextDisabled = false;
        $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].response = $scope.theResponse;
      }
    };

    $scope.canContinueUpdate = function(){
      //console.log('update');
      if($scope.theResponse !== ""){

        $scope.canContinue = true;
        $scope.nextDisabled = false;
      }else{
        $scope.canContinue = false;
        $scope.nextDisabled = false;
      }
    };

   // Unique Prev/Next Button Handler
   $scope.buttonClick = function(buttonType){

    if(buttonType === 'next'){
        //If there's more concerns, go to the next one
        if( ($scope.currentConcernIndex < $scope.concernsSelected.length-1) && $scope.canContinue){
          $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].response = $scope.theResponse;
          $scope.theResponse ="";
          nextConcern();
          $scope.theResponse = $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].response;
          $scope.canContinueUpdate();
        }
        else{
          //console.log('next: ' + $scope.nextPage);
          if($scope.canContinue){

            //if there's no concerns, don't try to update data
            if($scope.concernsSelected.length > 0){
              $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].response = $scope.theResponse;

            }
            concernsFactory.setPageComplete(currentPage);
            concernsFactory.updateRemote();
            concernsFactory.setCurrentPage($scope.nextPage);
            $location.path('/' + $scope.nextPage);
          }
          else{
            $scope.nextDisabled = true;
          }

        }
      }
      else if(buttonType === 'prev'){

        //If there's more concerns, go to the next one
        if( $scope.currentConcernIndex > 0){
          prevConcern();
          $scope.theResponse = $scope.question.concerns[$scope.concernsSelected[$scope.currentConcernIndex]].response;
          $scope.canContinueUpdate();
        }
        else{
          concernsFactory.setCurrentPage($scope.prevPage);
          $location.path('/' + $scope.prevPage);
        }
      }

    };

    $scope.returnClick = function(){
      concernsFactory.setCurrentPage('5');
      $location.path('/' + '5');
    };

    $scope.continueClick = function(){
      concernsFactory.setCurrentPage('8a');
      $location.path('/' + '8a');
    };

  }])

  ////////////////////////////
  // Summary Controller
  ////////////////////////////
  .controller('summaryCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', function ($scope, $location, donorIDFactory, concernsFactory) {
    //console.log('text');
    $scope.solution = {selected : null};
    $scope.theResponse = $scope.question.response;


    $scope.solutionClick = function(option){
      if(option === true){
        $scope.$parent.nextPage = "10"; //9 has been removed
        $scope.solution.selected = true;
      }
      else if(option === false){
        $scope.$parent.nextPage = "8c";
        $scope.solution.selected = false;

      }
      $scope.$parent.canContinue = true;
      $scope.$parent.nextDisabled = false;
    }



  }])


  ///////////////////////////////
  // Best Response Controller
  //////////////////////////////
  .controller('bestSolutionsCtrl', ['$scope', '$location','concernsFactory', function ($scope, $location,concernsFactory) {

    $scope.solutionClick = function(){
      $scope.canContinue = true;
      $scope.nextDisabled = false;
      //$scope.concerns[$scope.currentConcern].bestSolution = id;
      //console.log('best solution: ' + $scope.concerns[$scope.currentConcern].bestSolution);
    };

    //
    // Special next/prev button behaviour
    //
    $scope.buttonClick = function(buttonType){
      var currentPage = parseInt($location.path().substring(1) || 1);
        ////console.log('currentConcern: ' + $scope.currentConcern + ' clicked: '+ buttonType);

        // Next
        if(buttonType === 'next'){

          if($scope.concerns[$scope.currentConcern].solutionsSelected <= 1){
            // There is only a default solution, so let the user continue
            $scope.canContinue = true;
          }

          if($scope.canContinue){
            $scope.currentConcern += 1;

            $scope.canContinue = false;
            $scope.nextDisabled = false;

            while($scope.currentConcern <= 4){
              if($scope.concerns[$scope.currentConcern].selected){
                break;
              }
              else{
                //console.log($scope.currentConcern + ' was not selected' );
                $scope.currentConcern += 1;
              }
            }

            if($scope.currentConcern > 4){ //are there concerns left?
              //Done with concerns, next page.

              concernsFactory.setPage(currentPage);
              concernsFactory.updateRemote();
              $location.path('/' + String(currentPage + 1));
            }
          }
          else{
            if(!$scope.concerns[0].selected && !$scope.concerns[1].selected && !$scope.concerns[2].selected && !$scope.concerns[3].selected && !$scope.concerns[4].selected){
              //No concerns were selected

              //Done with concerns, next page.
              concernsFactory.setPage(currentPage);
              concernsFactory.updateRemote();
              $location.path('/' + String(currentPage + 1));
            }
            else{
              // Not all solutions have been selected yet, stay here
              $scope.nextDisabled = !$scope.canContinue;
            }

          }

        }
        //Previous
        else if(buttonType === 'prev'){

          if($scope.currentConcern > 0){
            $scope.currentConcern -= 1;

            $scope.canContinue = true;
            $scope.nextDisabled = false;

            //was this concern selected?
            if(!$scope.concerns[$scope.currentConcern].selected){
              $scope.buttonClick('prev');
            }
          }
          else{
            $location.path('/' + String(currentPage - 1));
          }
        }
      };

      $scope.currentConcern = 0;

      while($scope.currentConcern < 4 && (!$scope.concerns[$scope.currentConcern].selected) ){
        //console.log('concern ' + $scope.currentConcern + ' selected = ' + (!$scope.concerns[$scope.currentConcern].selected));
        $scope.currentConcern ++;
      }

      $scope.nextDisabled = false;

      if($scope.concerns[$scope.currentConcern].bestSolution === null){
        $scope.canContinue = false;
      }else{
        //console.log("Concerns best solution has already been selected");
        $scope.canContinue = true;
      }

    }])

  ///////////////////////////////
  // Comments Controller
  //////////////////////////////
  .controller('commentsCtrl', ['$scope', '$location','concernsFactory', function ($scope, $location,concernsFactory) {

    $scope.submitted = false;

    $scope.submitClick = function(){
      concernsFactory.updateComments($scope.comments);
      concernsFactory.setPage(parseInt($location.path().substring(1) || 1));
      concernsFactory.updateRemote();
      $scope.submitted = true;
    };

  }])

 ///////////////////////////////
  // Management login Controller
  //////////////////////////////

  .controller('manageLoginCtrl', ['$scope', '$location' ,'manageFactory', 'concernsFactory', function ($scope, $location, manageFactory, concernsFactory) {
    function init(){
      $scope.logInFailed = false;
      $scope.manageLoggedIn = manageFactory.loggedIn();
      $scope.theData ='';
    }

    $scope.submitClick = function(){
      //check username/pass
      manageFactory.logIn($scope.manageUser, $scope.managePass)
      .then(function(isLoggedIn){
        // console.log("is logged in: " + isLoggedIn);
        if(isLoggedIn == true){
          // console.log('login successful');
          $scope.manageLoggedIn = isLoggedIn;
          $location.path('/manage');
        }
        else{
          $scope.logInFailed = true;
        }
      });

    };

    init();

  }])


  ///////////////////////////////
  // Management Controller
  //////////////////////////////

  .controller('manageCtrl', ['$scope', '$location', 'donorIDFactory', 'concernsFactory', 'manageFactory', function ($scope, $location, donorIDFactory, concernsFactory, manageFactory) {
    function init(){
      $scope.orderBy = 'donor_id';
      $scope.addingNew = false;
      $scope.warningText = '';
      $scope.donorIDs = [];
      $scope.locations = [];
      loadRemoteData();
      // loadLocations();

    }

    $scope.$on('$viewContentLoaded', function(){
      loadRemoteData();
    });

    $scope.addDonorID = function(newID, newLocation){
      //console.log('adding ID: ' + newID + ' location: ' + newLocation);

      if(!doesIdExist(newID)){
        // send id off to the server
        donorIDFactory.post(newID, newLocation)
        .then(loadRemoteData());
        $scope.addingNew = false;
        $scope.warningText = "";
      }else{
        $scope.warningText = "ID: " + newID + " already exists";
      }
    };

    $scope.addLocation = function(newName, newUrl){
      //console.log('adding Location: ' + newName + ' url: ' + newUrl);
      // send id off to the server
      donorIDFactory.addLocation(newName, newUrl);
      $scope.addingNewLocation = false;

    };

    $scope.orderChange = function(orderVar){
      if($scope.orderBy !== orderVar){
        $scope.orderBy = orderVar;
      }
      else{
        $scope.orderBy = '-'+orderVar;
      }
    };

    function loadRemoteData() {
      //console.log('updating ID list');
      //get all the donor IDs
      donorIDFactory.getAll()
      .then(function(newIDs){
        //update the view
        $scope.donorIDs = newIDs.donor;
      });
    };

    function loadLocations(){
      //console.log('loading all locations');
      donorIDFactory.getAllLocations()
      .then(function(newLocations){
        angular.extend($scope.locations, newLocations.locations);
      // $scope.locations = newLocations.locations;
    });

    };

    function doesIdExist(toFind){
      console.log("checking for: " + toFind);
      for(var theID in $scope.donorIDs){
        if(toFind == theID){
          console.log(theID + "matchs" + $scope.donorIDs);
          return false;
        }
      }

      return true;
    }

    $scope.$on('$viewContentLoaded', function(){
      $scope.manageLoggedIn = manageFactory.loggedIn();
      $scope.level = manageFactory.level();
      //console.log('loggedIn: ' + $scope.loggedIn);

      if( !$scope.manageLoggedIn ){
        $location.path('/manage-login');
      }
    });

    init();

  }])

///////////////////////////////
  // Location Controller
  //////////////////////////////

  .controller('locationsCtrl', ['$scope', '$location', '$route', 'donorIDFactory', 'concernsFactory', 'manageFactory', function ($scope, $location, $route, donorIDFactory, concernsFactory, manageFactory) {
    function init(){
      $scope.orderBy = 'donor_id';
      $scope.addingNew = false;
      $scope.donorIDs = [];
      $scope.locations = [];
      // loadLocations();

    }

    $scope.$on('$viewContentLoaded', function(){
      // loadLocations();
    });

    $scope.addDonorID = function(newID){
      //console.log('adding ID: ' + newID + ' location: ' + newLocation);
      // send id off to the server
      donorIDFactory.post(newID)
      .then(loadRemoteData());
      $scope.addingNew = false;
    };

    $scope.addLocation = function(newName, newUrl){
      //console.log('adding Location: ' + newName + ' url: ' + newUrl);
      // send id off to the server
      donorIDFactory.addLocation(newName, newUrl).then(loadLocations());
      $scope.addingNew = false;


    };

    $scope.deleteClick = function(id){
      donorIDFactory.removeLocation(id).then(function(theResponse){
        loadLocations();
        $scope.$apply();
      });

    };

    $scope.refresh = function(){
      // loadLocations();
    }

    function loadLocations(){
      //console.log('loading all locations');
      donorIDFactory.getAllLocations()
      .then(function(newLocations){
        $scope.locations = newLocations.locations;
      });

    }

    $scope.$on('$viewContentLoaded', function(){
      $scope.manageLoggedIn = manageFactory.loggedIn();
      ////console.log('loggedIn: ' + $scope.loggedIn);

      if( !$scope.manageLoggedIn ){
        console.log('not logged in');
        $location.path('/manage-login');
      }
    });

    init();

  }])

  ///////////////////////////////
  // Donor Controller
  //////////////////////////////
  .controller('donorDetailCtrl', ['$scope', 'donorIDFactory', '$route', '$routeParams', '$location', 'manageFactory', function ($scope, donorIDFactory, $route, $routeParams, $location, manageFactory) {
    function init(){
      $scope.locations = [];
      $scope.donorID = $routeParams.id;
      $scope.changingLocation = false;
      $scope.theLocation;
      loadRemoteData();
      // loadLocations();
    }


    function loadRemoteData() {
      //get all the donor IDs
      donorIDFactory.get($scope.donorID)
      .then(function(newID){
        //update the view
        $scope.donor = newID.donor[0];
      });

    }

    function loadLocations(){
      //console.log('loading all locations');
      donorIDFactory.getAllLocations()
      .then(function(newLocations){
        angular.extend($scope.locations, newLocations.locations);
      // $scope.locations = newLocations.locations;
    });

    }

    $scope.editLocationClick = function(){
      //console.log('updating location');
      $scope.changingLocation = true;
    }

    $scope.submitLocationClick = function(theLocation){
      //console.log('location is: ' + theLocation.id);
      $scope.donor.location = theLocation.id;
      donorIDFactory.updateLocation(theLocation.id, $scope.donor.id);
      $scope.changingLocation = false;
    }

    $scope.deleteClick = function(){
      if(confirm('are you sure you want to delete donor: '+ $scope.donorID + '?')){
        //console.log('deleting user: ' + $scope.donorID + " " + $scope.donor.id);
        donorIDFactory.remove($scope.donor.id)
        .then(function(){
          $location.path('/manage');
        });
      }
    };

    $scope.$on('$viewContentLoaded', function(){
      $scope.manageLoggedIn = manageFactory.loggedIn();
      ////console.log('loggedIn: ' + $scope.loggedIn);

      if( !$scope.manageLoggedIn ){
        console.log('not logged in');
        $location.path('/manage-login');
      }
    });

    init();

  }])

//////////////////////////////////////////////////////////////////////////////
// Services
//////////////////////////////////////////////////////////////////////////////
.factory('manageFactory', ['$http', function($http){
  var loggedIn = false;
  var level = 0;

  function isLoggedIn(){
    if(sessionStorage.getItem('username') && sessionStorage.getItem('pass') && loggedIn == false){
      console.log('already logged in: ' + sessionStorage.getItem('username') +', ' + sessionStorage.getItem('pass'));
      logIn(sessionStorage.getItem('username'), sessionStorage.getItem('pass'));
    }
    return loggedIn;
  }

  function getLevel(){
    return level;
  }

    //
    // check pass
    //
    function logIn(user, pass){
      //console.log('logging in');
      var theData = {"username":user, "pass":pass};
      var request = $http({
        method: "POST",
        url: "/api/pass.php/",
        data: theData
      });

      return( request.then( handleSuccess, handleError ) );
    }

    // I transform the error response, unwrapping the application dta from
    // the API response payload.
    function handleError( response ) {
      //console.log('Unknown Error');
      return response;
    }
    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess( response ) {
      //console.log('setting loggedIn to: ' + response.data);
      loggedIn = response.data.successful;
      level = response.data.level;
      console.log(response.data);

      // if(loggedIn == true){
      //   sessionStorage.setItem('username');
      //   sessionStorage.setItem('pass');
      // }

      //console.log('success');
      return( response.data.successful );
    }


    return({
      logIn     : logIn,
      loggedIn  : isLoggedIn,
      level     : getLevel
    });

  }])

//////////////////////////////////////////////////////////

.factory('donorIDFactory', ['$http', function($http){

  var loggedIn = false;
  var donorID = '';
  var dbID = '';

  var location =
  {
    "id" : 1,
    "name" : "NYBC",
    "url" : "http://nybc.org/"
  };

  function getDonorID(){
    return donorID;
  }

  function getDataID(){
    return dbID;
  }

  function logIn(careID, dataID){
    donorID = careID;
    dbID = dataID;
    loggedIn = true;
    sessionStorage.setItem('donorID', donorID);
    return loggedIn;
  }

  function isLoggedIn(){
    return loggedIn;
  }

    //
    // Get all user ids
    //
    function getAllDonorData(){
      var request = $http({
        method: "GET",
        url:  "https://mydonationaus.org/api/api.php/donor/?columns=id,donor_id,date_added,completed",

      });

      return( request.then( handleSuccess, handleError ) );
    }



    //
    // Get a user id data
    //
    function getDonorData(id){
      var request = $http({
        method: "GET",
        url:  "https://mydonationaus.org/api/api.php/donor/?filter=donor_id,eq,"+ id

      });

      return( request.then( handleSuccess, handleError ) );
    }

    function setLocation(locationId){
      //console.log("getting location info: " + locationId);

      var request = $http({
        method: "GET",
        url:  "https://mydonationaus.org/api/api.php/locations/" + locationId

      });

      request.then( function(theLocation){
        location = theLocation.data;
      }, handleError );
    }

    function getLocation(){
      return location;
    }

    function getAllLocations(){
      var request = $http({
        method: "GET",
        url:  "https://mydonationaus.org/api/api.php/locations/"

      });

      return( request.then( handleSuccess, handleError ) );

    }


    //
    // Create an id
    //
    function addDonorID(newID, newLocation){
      var request = $http({
        method: "POST",
        url: "https://mydonationaus.org/api/api.php/donor/",
        data: {donor_id: newID, location: newLocation}
      });

      return( request.then( handleSuccess, handleError ) );
    }

    //
    // Update a location
    //
    function updateLocation(newLocation, theDonorID){
      //console.log("https://mydonationaus.org/api/api.php/donor/" + theDonorID);
      var request = $http({
        method: "PUT",
        url: "https://mydonationaus.org/api/api.php/donor/" + theDonorID + "/",
        data: {"location": newLocation}
      });

      return( request );
    }

    //
    // Create a location
    //
    function addLocation(newLocationName, newLocationUrl){
      var request = $http({
        method: "POST",
        url: "https://mydonationaus.org/api/api.php/locations/",
        data: {name: newLocationName, url: newLocationUrl}
      });

      return( request );
    }

       //
    // Delete an id
    //
    function removeLocation(id){
      var request = $http({
        method: "DELETE",
        url: "https://mydonationaus.org/api/api.php/locations/"+id,
      });

      return( request );
    }

   //
    // Delete an id
    //
    function removeDonorID(id){
      var request = $http({
        method: "DELETE",
        url: "https://mydonationaus.org/api/api.php/donor/"+id,
      });

      return( request.then( handleSuccess, handleError ) );
    }

    // I transform the error response, unwrapping the application dta from
    // the API response payload.
    function handleError( response ) {
      //console.log('Unknown Error');
      return response;
    }
    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess( response ) {
      if(response.config.method === "GET"){
        //console.log('it was a GET!');
        return( phpCrudApiTransform(response.data) );
      }else if(response.config.method === "POST"){
        //console.log('it was a POST!');
        return( response.data );
      }
    }


    //
    // Format the JSON from the server
    //
    function phpCrudApiTransform(tables) {
      var arrayFlip = function (trans) {
        var key, tmpAr = {};
        for (key in trans) {
          tmpAr[trans[key]] = key;
        }
        return tmpAr;
      };
      var getObjects = function (tables,tableName,whereIndex,matchValue) {
        var objects = [];
        for (var record in tables[tableName].records) {
          record = tables[tableName].records[record];
          if (!whereIndex || record[whereIndex]===matchValue) {
            var object = {};
            for (var index in tables[tableName].columns) {
              var column = tables[tableName].columns[index];
              object[column] = record[index];
              for (var relation in tables) {
                var reltable = tables[relation];
                for (var key in reltable.relations) {
                  var target = reltable.relations[key];
                  if (target === tableName+'.'+column) {
                    var columnIndices = arrayFlip(reltable.columns);
                    object[relation] = getObjects(tables,relation,columnIndices[key],record[index]);
                  }
                }
              }
            }
            objects.push(object);
          }
        }
        return objects;
      };
      var tree = {};
      for (var name in tables) {
        var table = tables[name];
        if (!table.relations) {
          tree[name] = getObjects(tables,name);
          if (table.results) {
            tree._results = table.results;
          }
        }
      }
      return tree;
    }

    return({
      getAll: getAllDonorData,
      get: getDonorData,
      remove: removeDonorID,
      getID: getDonorID,
      getDbID: getDataID,
      post: addDonorID,
      logIn: logIn,
      loggedIn : isLoggedIn,
      setLocation : setLocation,
      getLocation : getLocation,
      getAllLocations: getAllLocations,
      addLocation : addLocation,
      updateLocation : updateLocation,
      removeLocation : removeLocation
    });
  }])

  /*******************************************
  * Concerns factory
  *******************************************/
  .factory('concernsFactory',['$http','donorIDFactory',  function($http ,donorIDFactory){
    var currentPage = 'welcome';

    var location = 0;
    var completed = false;

    var pages = [
    {'pageNum' : 1,
    'complete' : 0},
    {'pageNum' : 2,
    'complete' : 0},
    {'pageNum' : 3,
    'complete' : 0},
    {'pageNum' : 4,
    'complete' : 0},
    {'pageNum' : 5,
    'complete' : 0},
    {'pageNum' : 6,
    'complete' : 0},
    {'pageNum' : 7,
    'complete' : 0},
    {'pageNum' : 8,
    'complete' : 0},
    {'pageNum' : 9,
    'complete' : 0},
    {'pageNum' : 10,
    'complete' : 0},
    {'pageNum' : 11,
    'complete' : 0},
    {'pageNum' : 12,
    'complete' : 0}
    ];


    var questions = [
    {
      "id": "0",
      "type" : "text",
      "reflections" : null,
      "next" : "1",
      "prev" : "",
      "canContinue" : true,
    },
    {
      "id": "1",
      "type" : "text",
      "text" : "Why have you donated blood in the past?",
      "reflections" : null,
      "next" : "1a",
      "prev" : "0",
      "response" : "",
      "canContinue" : false,
    },
    {
      "id": "1a",
      "type" : "multiple",
      "hasNoneOfTheAbove" : true,
      "text" : "Here are some reasons why others have donated in the past. Thinking back to your own reasons for donating, which of the following most closely resemble yours?",
      "next" : "1b",
      "prev" : "1",
      "canContinue" : false,
      "options" : [
      {
        "id" : 0,
        "text": "My blood can be used by everyone",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 1,
        "text": "One donation can save up to three lives",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 2,
        "text": "Someone I know has received a blood transfusion",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 3,
        "text": "Donating blood is something I can do with a friend or colleague",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 4,
        "text": "Donating blood is the right thing to do",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 5,
        "text": "Donating blood makes me feel good about myself",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 6,
        "text": "I may need a transfusion of O-Negative blood in the future",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 7,
        "text": "I donate blood because I know that demand for O-Negative blood is high",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 8,
        "text": "I donate blood because others do not donate blood",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 9,
        "text": "Other people I know donate blood",
        "selected" : null,
        "reflection" : 0
      }
      ],
      "reflections" : null,
      // "next" : "1b"
    },
    {
      "id": "1b",
      "type" : "text",
      "text" : "",
      "reflections" : null,
      "response" : "",
      "next" : "2",
      "prev" : "1a",
      "canContinue" : true,
    },
    {
      "id": "2",
      "type" : "text",
      "text" : "How is donating blood consistent with your values?",
      "reflections" : null,
      "response" : "",
      "next" : "2a",
      "prev" : "1b",
      "canContinue" : false,
    },
    {
      "id": "2a",
      "text" : "Here are some ways that other blood donors have described themselves. What does donating blood say about you?",
      "next" : "2b",
      "prev" : "2",
      "canContinue" : false,
      "options" : [
      {
        "id" : 0,
        "text": "I like to help others",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 1,
        "text": "I like to volunteer",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 2,
        "text": "I put others first before myself",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 3,
        "text": "I care about my community",
        "selected" : null,
        "reflection" : 0
      },
      {
        "id" : 4,
        "text": " I care about society",
        "selected" : null,
        "reflection" : 0
      },
      ]
    },
    {
      "id": "2b",
      "type" : "text",
      "text" : "",
      "reflections" : null,
      "response" : "",
      "next" : "3",
      "prev" : "2a",
      "canContinue" : true,
    },
    {
      "id": "3",
      "value" : 5,
      "next" : "3a",
      "prev" : "2a",
      "text" : "How important is it for you to donate blood?",
      "canContinue" : true,
    },
    {
      "id": "3a",
      "text": "Your rating of X suggests that donating blood is very important to you. What makes it so important for you?",
      "next" : "4",
      "prev" : "3",
      "altNext" : "3b",
      "canContinue" : false,
    },
    {
      "id": "3b",
      "text" : "What would it take to get you to a (X + 2)",
      "next" : "4",
      "prev" : "3a",
      "canContinue" : false,
    },
    {
      "id": "3c",
      "next" : "4",
      "prev" : "3b",
      "canContinue" : true,
    },
    {
      "id": "3d",
      "next" : "4",
      "prev" : "3",
      "canContinue" : true,
    },
    {
      "id": "4",
      "value" : 5,
      "next" : "4a",
      "prev" : "3a",
      "text" : "How confident are you that you will give blood again?",
      "canContinue" : true,
    },
    {
      "id": "4a",
      "next" : "5",
      "prev" : "4",
      "altNext" : "4b",
      "canContinue" : false,
    },
    {
      "id": "4b",
      "next" : "5",
      "prev" : "4",
      "text"  : "What would it take to get you to a {{questions[12].value + 2}}?",
      "canContinue" : false,
    },
    {
      "id": "4c",
      "next" : "5",
      "prev" : "4b",
      "canContinue" : true,
    },
    {
      "id": "4d",
      "next" : "5",
      "prev" : "4",
      "canContinue" : true,
    },
    {
      "id": "5",
      "next" : "5a",
      "prev" : "4a",
      "text" : "Do you have any of the following concerns about donating?",
      "canContinue" : false,
      "hasNoneOfTheAbove" : true,
      "options" : [
      {
        "id" : 0,
        "text": "Fear of needles or blood",
        "selected" : false,
        "reflection" : 0
      },
      {
        "id" : 1,
        "text": "Feeling faint, dizzy or lightheaded",
        "selected" : false,
        "reflection" : 0
      },
      {
        "id" : 2,
        "text": "Feeling tired after donating",
        "selected" : false,
        "reflection" : 0
      },
      {
        "id" : 3,
        "text": "Being able to access a place to donate that is local or convenient to me",
        "selected" : false,
        "reflection" : 0
      },
      {
        "id" : 4,
        "text": "Getting an appointment at a time that works with my schedule",
        "selected" : false,
        "reflection" : 0
      },

      ],
    },
    {
      "id": "5a",
      "next" : "5b",
      "prev" : "5",
      "canContinue" : true,
    },
    {
      "id": "5b",
      "next" : "6",
      "prev" : "5",
      "canContinue" : true,
    },
    {
      "id": "6",
      "next" : "7",
      "prev" : "5b",
      "canContinue" : false,
      "concerns"  : [
      {
        "id": "6a",
        "next" : "6b",
        "prev" : "5a",
        "canContinue" : true,
        "text" : "You indicated that you have a fear of <strong>blood or needles</strong>. Here are solutions that have been used by other donors. Could these work for you?",
        "options" : [
        {
          "id" : 0,
          "text": "Distract yourself with music, texting, reading, or talking to staff or other donors.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 1,
          "text": "Express your concerns to the staff when you arrive, so they can help you.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 2,
          "text": "Ask for an experienced staff member to draw your blood.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 3,
          "text": "Tell the staff which arm you prefer.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 4,
          "text": "Bring someone for support.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 5,
          "text": "Schedule an appointment at a less busy time to shorten your wait.",
          "selected" : null,
          "reflection" : 0
        },
        ],
      },
      {
        "id": "6b",
        "next" : "6c",
        "prev" : "5a",
        "text" : "You indicated that you have concerns about <strong>feeling faint, dizzy, or lightheaded</strong>. Here are solutions that have been used by other donors. Could these work for you?",
        "canContinue" : true,
        "options" : [
        {
          "id" : 0,
          "text": "Drink a bottle of water 10 minutes before you donate. Water causes a short-term blood pressure increase that helps prevent dizziness and faintness. It also helps replace the fluid that you lose during donation.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 1,
          "text": "Tense your leg and stomach muscles every 10 seconds while donating. Crossing your legs and tensing your leg and stomach muscles helps prevent faintness and dizziness by keeping blood pressure in normal range. ",
          "selected" : null,
          "reflection" : 0
        },
        ],
      },
      {
        "id": "6c",
        "next" : "6e",
        "prev" : "5a",
        "text" : "You indicated that you have concerns about <strong>feeling tired after donating</strong>. Here are solutions that have been used by other donors. Could these work for you?",
        "canContinue" : true,
        "options" : [
        {
          "id" : 0,
          "text": "Drink plenty of fluid and consume salty foods on the day before you donate. This helps to store fluid.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 1,
          "text": "Drink plenty of fluid on the day of your donation, both before and after you give blood.",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 2,
          "text": "Avoid alcohol and caffeine on the day of your donation as these dehydrate your body.",
          "selected" : null,
          "reflection" : 0
        },
        ],
      },
      {
        "id": "6e",
        "next" : "6f",
        "prev" : "5a",
        "text" : "You indicated that you have concerns about <strong>being able to access a place to donate that is local or convenient to me</strong>. Here are solutions that  have been used by other donors. Could these work for you?",
        "canContinue" : true,
        "options" : [
        {
          "id" : 0,
          "text": "Go to donateblood.com.au to find your nearest donation venue. If it is a mobile unit, then you may be able to see when the next sessions are so you can put that into your schedule now.",
          "location" : '',
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 1,
          "text": "Call the Blood Service on 13 14 95 to find your nearest donation venue. If it is a mobile unit, then we may be able to tell you when the next sessions are so you can put that into your schedule now.",
          "location" : '',
          "selected" : null,
          "reflection" : 0
        },
        ],
      },
      {
        "id": "6f",
        "next" : "7/a/",
        "prev" : "5a",
        "canContinue" : true,
        "text" : "You indicated that you have concerns about <strong>Getting an appointment at a time that works with my schedule</strong>. Here are solutions that have been used by other donors. Could these work for you?",
        "options" : [
        {
          "id" : 0,
          "text": "Check your schedule to see when you are free over the next month and call the Blood Service on 13 14 95 to book an appointment at your local donation venue",
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 1,
          "text": "Take your schedule with you to your next donation and book in another appointment when you next present to donate",
          "location" : '',
          "selected" : null,
          "reflection" : 0
        },
        {
          "id" : 2,
          "text": "Create a personal reminder in your calendar or diary to book in an appointment either online or by ringing 13 14 95. ",
          "selected" : null,
          "reflection" : 0
        },
        ],
      },
      ]
    },
    {
      "id": "7",
      "next" : "8a",
      "prev" : "6",
      "canContinue" : false,
      "concerns"  : [
      {
        "id" : 0,
        "text": "your fear of needles or blood",
        "selected" : false,
        "response" : "",
      },
      {
        "id" : 1,
        "text": "feeling faint, dizzy or lightheaded",
        "selected" : false,
        "response" : ""
      },
      {
        "id" : 2,
        "text": "feeling tired after donating",
        "selected" : false,
        "response" : ""
      },
      {
        "id" : 3,
        "text": "finding a place to donate",
        "selected" : false,
        "response" : ""
      },
      {
        "id" : 4,
        "text": "finding a time to donate",
        "selected" : false,
        "response" : ""
      },
      {
        "id" : 5,
        "text": "finding a time to donate",
        "selected" : false,
        "response" : ""
      },
      ],
    },
    {
      "id": "8a",
      "next" : "8c",
      "prev" : "7",
      "canContinue" : false,
      "selection" : null
    },
    {
      "id": "8c",
      "next" : "10",
      "prev" : "8a",
      "response" : "",
      "text" : "I'm sorry. Can you please clarify for me what was incorrect so that I may better understand your reasons for giving blood and your concerns?",
      "canContinue" : false,
    },
    {
      "id": "9", //removed
      "next" : "10",
      "prev" : "8a",
      "response" : "",
      "text" : "Thank you for taking the time to visit MyDonation. Please provide your email address so that we may send you a summary of the information that was covered today. ",
      "canContinue" : false,
    },
    {
      "id": "10",
      "type" : "text",
      "text" : "Thank you for taking the time to visit MyDonation. We welcome any comments that you might have and invite you to share them below.",
      "reflections" : null,
      "next" : "11",
      "prev" : "8a",
      "response" : "",
      "canContinue" : true,
    },
    {
      "id": "11",
      "type" : "text",
      "text" : "We welcome any comments that you might have and invite you to share them below.",
      "reflections" : null,
      "next" : "",
      "prev" : "10",
      "response" : "",
      "canContinue" : true,
    },


    ];

    function getAll(){
      return questions;
    }

    function updateLocations(){

    }

    function updateLocalData(theData){
      //console.log('Updating Concern Data for: ' + theData.donor_id);
      // concerns[0].selected = convertToBool(theData.concern1);


      // if(theData.concern1_best !== null){
      //   concerns[0].bestSolution = parseInt(theData.concern1_best);
      // }
      questions[1].response = theData.q1;;
      questions[2].options[0].selected = convertToBool(theData.q1a_a1);
      questions[2].options[1].selected = convertToBool(theData.q1a_a2);
      questions[2].options[2].selected = convertToBool(theData.q1a_a3);
      questions[2].options[3].selected = convertToBool(theData.q1a_a4);
      questions[2].options[4].selected = convertToBool(theData.q1a_a5);
      questions[2].options[5].selected = convertToBool(theData.q1a_a6);
      questions[2].options[6].selected = convertToBool(theData.q1a_a7);
      questions[2].options[7].selected = convertToBool(theData.q1a_a8);
      questions[2].options[8].selected = convertToBool(theData.q1a_a9);
      questions[2].options[9].selected = convertToBool(theData.q1a_a10);
      questions[4].response = theData.q2;
      questions[5].options[0].selected = convertToBool(theData.q2a_a1);
      questions[5].options[1].selected = convertToBool(theData.q2a_a2);
      questions[5].options[2].selected = convertToBool(theData.q2a_a3);
      questions[5].options[3].selected = convertToBool(theData.q2a_a4);
      questions[5].options[4].selected = convertToBool(theData.q2a_a5);
      questions[7].value = Number(theData.q3);
      questions[8].response = theData.q3a;
      questions[9].response = theData.q3b;
      questions[10].response = theData.q3c;
      questions[11].response = theData.q3d;
      questions[12].value = Number(theData.q4);
      questions[13].response = theData.q4a;
      questions[14].response = theData.q4b;
      questions[17].options[0].selected = convertToBool(theData.q5_a1);
      questions[17].options[1].selected = convertToBool(theData.q5_a2);
      questions[17].options[2].selected = convertToBool(theData.q5_a3);
      questions[17].options[3].selected = convertToBool(theData.q5_a4);
      questions[17].options[4].selected = convertToBool(theData.q5_a5);
      questions[20].concerns[0].options[0].selected = convertToBool(theData.q6a_a1);
      questions[20].concerns[0].options[1].selected = convertToBool(theData.q6a_a2);
      questions[20].concerns[0].options[2].selected = convertToBool(theData.q6a_a3);
      questions[20].concerns[0].options[3].selected = convertToBool(theData.q6a_a4);
      questions[20].concerns[0].options[4].selected = convertToBool(theData.q6a_a5);
      questions[20].concerns[0].options[5].selected = convertToBool(theData.q6a_a6);
      questions[20].concerns[1].options[0].selected = convertToBool(theData.q6b_a1);
      questions[20].concerns[1].options[1].selected = convertToBool(theData.q6b_a2);
      questions[20].concerns[2].options[0].selected = convertToBool(theData.q6c_a1);
      questions[20].concerns[2].options[1].selected = convertToBool(theData.q6c_a2);
      questions[20].concerns[2].options[2].selected = convertToBool(theData.q6c_a3);
      // questions[20].concerns[3].options[0].selected = convertToBool(theData.q6d_a1);
      // questions[20].concerns[3].options[1].selected = convertToBool(theData.q6d_a2);
      // questions[20].concerns[3].options[2].selected = convertToBool(theData.q6d_a3);
      // questions[20].concerns[3].options[3].selected = convertToBool(theData.q6d_a4);
      questions[20].concerns[3].options[0].selected = convertToBool(theData.q6d_a1);
      questions[20].concerns[3].options[1].selected = convertToBool(theData.q6d_a2);
      questions[20].concerns[4].options[0].selected = convertToBool(theData.q6e_a1);
      questions[20].concerns[4].options[1].selected = convertToBool(theData.q6e_a2);
      questions[20].concerns[4].options[2].selected = convertToBool(theData.q6e_a3);
      questions[21].concerns[0].response = theData.q7a;
      questions[21].concerns[1].response = theData.q7b;
      questions[21].concerns[2].response = theData.q7c;
      questions[21].concerns[3].response = theData.q7d;
      questions[21].concerns[4].response = theData.q7e;
      questions[21].concerns[5].response = theData.q7f;
      questions[23].response = theData.q8c;
      questions[24].response = theData.q9;
      questions[25].response = theData.q10;
      completed = theData.completed;
      location = theData.location;
    }

    function updateRemoteData(){

      var theID = donorIDFactory.getDbID();

      var updated = updateTime();

      var request = $http({
        method: "PUT",
        url: "https://mydonationaus.org/api/api.php/donor/" + theID + "/",
        data: {
          "q1": questions[1].response,
          "q1a_a1": questions[2].options[0].selected,
          "q1a_a2": questions[2].options[1].selected,
          "q1a_a3": questions[2].options[2].selected,
          "q1a_a4": questions[2].options[3].selected,
          "q1a_a5": questions[2].options[4].selected,
          "q1a_a6": questions[2].options[5].selected,
          "q1a_a7": questions[2].options[6].selected,
          "q1a_a8": questions[2].options[7].selected,
          "q1a_a9": questions[2].options[8].selected,
          "q1a_a10": questions[2].options[9].selected,
          "q2": questions[4].response,
          "q2a_a1": questions[5].options[0].selected,
          "q2a_a2": questions[5].options[1].selected,
          "q2a_a3": questions[5].options[2].selected,
          "q2a_a4": questions[5].options[3].selected,
          "q2a_a5": questions[5].options[4].selected,
          "q3": questions[7].value,
          "q3a": questions[8].response,
          "q3b": questions[9].response,
          "q3c": questions[10].response,
          "q3d": questions[11].response,
          "q4": questions[12].value,
          "q4a": questions[13].response,
          "q4b": questions[14].response,
          "q5_a1": questions[17].options[0].selected,
          "q5_a2": questions[17].options[1].selected,
          "q5_a3": questions[17].options[2].selected,
          "q5_a4": questions[17].options[3].selected,
          "q5_a5": questions[17].options[4].selected,
          "q6a_a1": questions[20].concerns[0].options[0].selected,
          "q6a_a2": questions[20].concerns[0].options[1].selected,
          "q6a_a3": questions[20].concerns[0].options[2].selected,
          "q6a_a4": questions[20].concerns[0].options[3].selected,
          "q6a_a5": questions[20].concerns[0].options[4].selected,
          "q6a_a6": questions[20].concerns[0].options[5].selected,
          "q6b_a1": questions[20].concerns[1].options[0].selected,
          "q6b_a2": questions[20].concerns[1].options[1].selected,
          "q6c_a1": questions[20].concerns[2].options[0].selected,
          "q6c_a2": questions[20].concerns[2].options[1].selected,
          "q6c_a3": questions[20].concerns[2].options[2].selected,
          // "q6d_a1": questions[20].concerns[3].options[0].selected,
          // "q6d_a2": questions[20].concerns[3].options[1].selected,
          // "q6d_a3": questions[20].concerns[3].options[2].selected,
          // "q6d_a4": questions[20].concerns[3].options[3].selected,
          "q6d_a1": questions[20].concerns[3].options[0].selected,
          "q6d_a2": questions[20].concerns[3].options[1].selected,
          "q6e_a1": questions[20].concerns[4].options[0].selected,
          "q6e_a2": questions[20].concerns[4].options[1].selected,
          "q6e_a3": questions[20].concerns[4].options[2].selected,
          "q7a": questions[21].concerns[0].response,
          "q7b": questions[21].concerns[1].response,
          "q7c": questions[21].concerns[2].response,
          "q7d": questions[21].concerns[3].response,
          "q7e": questions[21].concerns[4].response,
          "q7f": questions[21].concerns[5].response,
          "q8c": questions[23].response,
          "q9": questions[24].response,
          "q10": questions[25].response,
          "completed" : completed,
            // "comments" : comments,
            "last_updated" : updated
          }
        });

    }

    function getCurrentPage(){
      return currentPage;
    }

    function setCurrentPage(newPage){
      //console.log('setting currentPage to: ' + newPage);
      currentPage = newPage;
    }

    function setPageComplete(pageNum){
        // //console.log('finished page: ' + pageNum);
        // pages[pageNum-1].complete = 1;
      }

      function setComments(theComments){
        comments = theComments;
      }

      function updateTime(){
        var lastUpdated = new Date();
        //console.log('lastUpdated: ' + lastUpdated);
        return lastUpdated;
      }

      function selectedCounter(i){
        var count = 0;
        for (var j = concerns[i].solutions.length - 1; j >= 0; j--) {
          if(concerns[i].solutions[j].selected){
            count++;
          }
        }
        return count;
      }

      function convertToBool(theInt){
        switch(theInt){
          case null:
          return null;
          case 0:
          case '0':
          case "0":
          return false;
          case 1:
          case '1':
          case "1":
          return true;
        }

      }

      function setCompleted(){
        completed = true;
      }

      return({
        all: getAll,
        update: updateLocalData,
        updateRemote: updateRemoteData,
        setPageComplete: setPageComplete,
        updateComments: setComments,
        getCurrentPage: getCurrentPage,
        setCurrentPage: setCurrentPage,
        setCompleted: setCompleted
      });

    }]);
