'use strict';

angular.module('careApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/welcome.html',
        controller: 'loginCtrl'
      })
      .when('/1', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/1a', {
        templateUrl: 'views/multi.html',
        controller: 'multiCtrl'
      })
      .when('/1b', {
        templateUrl: 'views/1b.html'
      })
      .when('/2', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/2a', {
        templateUrl: 'views/multi.html',
        controller: 'multiCtrl'
      })
      .when('/2b', {
        templateUrl: 'views/2b.html'
      })
      .when('/3', {
        templateUrl: 'views/slider.html',
        controller: 'sliderCtrl'
      })
      .when('/3a', {
        templateUrl: 'views/3a.html',
        controller: 'sliderTextCtrl'
      })
      .when('/3b', {
        templateUrl: 'views/3b.html',
        controller: 'textCtrl'
      })
      .when('/3c', {
        templateUrl: 'views/3c.html'
      })
      .when('/3d', {
        templateUrl: 'views/3d.html'
      })
      .when('/4', {
        templateUrl: 'views/slider.html',
        controller: 'sliderCtrl'
      })
      .when('/4a', {
        templateUrl: 'views/4a.html',
        controller: 'sliderTextCtrl'
      })
      .when('/4b', {
        templateUrl: 'views/4b.html',
        controller: 'textCtrl'
      })
      .when('/4c', {
        templateUrl: 'views/4c.html'
      })
      .when('/4d', {
        templateUrl: 'views/4d.html'
      })
      .when('/5', {
        templateUrl: 'views/multi.html',
        controller: 'multiCtrl'
      })
      .when('/5a', {
        templateUrl: 'views/5a.html',
      })
      .when('/5b', {
        templateUrl: 'views/5b.html',
      })
      .when('/6', {
        templateUrl: 'views/solution.html',
        controller: 'solutionsCtrl'
      })
      .when('/7', {
        templateUrl: 'views/solution-response.html',
        controller: 'solutionsResponseCtrl'
      })
      .when('/8a', {
        templateUrl: 'views/8a.html',
        controller: 'summaryCtrl'
      })
      .when('/8c', {
        templateUrl: 'views/8c.html',
        controller: 'textCtrl'
      })
      .when('/9', {
        templateUrl: 'views/9.html',
        controller: 'textCtrl'
      })
      .when('/10', {
        templateUrl: 'views/comments.html',
        controller: 'textCtrl'
      })
      .when('/11', {
        templateUrl: 'views/11.html',
        controller: "creditCtrl",
      })
      .when('/manage', {
        templateUrl: 'views/manage.html',
        controller: 'manageCtrl'
      })
      .when('/locations', {
        templateUrl: 'views/locations.html',
        controller: 'locationsCtrl'
      })
      .when('/manage/id/:id', {
        templateUrl: 'views/donordetail.html',
        controller: 'donorDetailCtrl'
      })
      .when('/manage-login', {
        templateUrl: 'views/manage-login.html',
        controller: 'manageLoginCtrl'
      })
      .when('/manage/id/:id', {
        templateUrl: 'views/donordetail.html',
        controller: 'donorDetailCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

  });
